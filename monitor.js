const
// main includes
  fs = require('fs'),
  https = require('https'),
  http = require('http'),
  express = require('express'),
// main utilities
  _ = require('lodash'),
  moment = require('moment'),
  Events = require('events'),
  assert = require('assert'),
// tobe moved to modules
  Conf = require('node-conf');

// Load configuration
var
  eventEmitter = new Events.EventEmitter(),
  node_env = process.env.NODE_ENV ? process.env.NODE_ENV : 'development',
  config = Conf.load(node_env);

if (!config.port
  || !config.secret
  || !config.host
  || !config.mongo
  || !config.mongo.name
  || !config.mongo.host
  || !config.mongo.port
  || !config.mongo.user) {
  return console.log('\u001b[31mMissing configuration file \u001b[33mconfig/' +
    node_env +
    '.json\u001b[31m. Create configuration file or start with `NODE_ENV=development node monitor.js` to use another configuration file.\033[0m');
}

var app = express(),
    credentials = {
      key: fs.readFileSync('./privkey.pem'),
      cert: fs.readFileSync('./fullchain.pem')
    },
    Https = https.createServer(credentials, app),
    Http = http.Server(app);

// load modules
var notify = require('./server/notify')(config, eventEmitter);
var api = require('./server/api')(config, eventEmitter, Https);
var persistence = require('./server/persistence_arangodb')(config, eventEmitter);
var collector = require('./server/collector')(config, eventEmitter);


Http.listen(config.port, function () {
  var msg = 'Monitor app started, listening on port:' + config.port;
  console.log(msg);
});

app.use(function(req, res, next) {
  if(!req.secure) {
    return res.redirect(['https://', req.hostname, ':3443', req.url].join(''));
  }
  next();
});
app.use(express.static('client'));

Https.listen(config.port_ssl, function () {
  var msg = 'Monitor app started, listening on port:' + config.port_ssl;
  console.log(msg);
});

persistence.init(function () {

  api.init(persistence);

  collector.init(persistence);

}, function (err) {

  console.log('unable to connect to Mongo', err);

});
