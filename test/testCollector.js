/**
 * Created by alex on 6/01/16.
 */
var assert = require('assert');
var util=require('util');
var _ = require('lodash');
var collector = require('../server/collector')({});

var metrics = [
  {name: 'outCurrent', granularities: ['10seconds', 'minute'], operations: ['average', 'total', 'max']},
  {name: 'inCurrent', granularities: ['minute'], operations: ['average', 'min', 'count']},
  {name: 'solar', granularities: ['minute'], operations: ['average', 'max',]},,
  {
    name: 'solarPower',
    calculation: {
      args: ['solar', 'outVoltage'],
      pred: 'return (solar * outVoltage) / 1000'
    },
    granularities: ['minute', 'hour', 'day', 'week', 'month'],
    operations: ['average', "totalhour"]
  }
];

var testReading = {
  inCurrent: 10,
  outCurrent: 20
};

var testReading2 = {
  inCurrent: 20,
  outCurrent: 8
};

describe('Collector', function () {

  describe('initializeDoc', function () {

    it('should return a valid document', function () {
      var initialized = collector._initializeDoc(metrics, testReading);

      assert.equal(initialized.inCurrent.avg.val, 10);
      assert.equal(initialized.outCurrent.avg.val, 20);

    });

  });

  describe('calculateMetric', function() {

    it('should calculate correct next values', function() {

      var initialized = collector._initializeDoc(metrics, testReading);
      console.log(util.inspect(initialized, {showHidden: false, depth: null}));

      var next = collector._calculateMetric(testReading2, initialized);
      assert.equal(next.outCurrent.avg.val, 14);
      assert.equal(next.outCurrent.tot.val, 28);
      assert.equal(next.outCurrent.max.val, 20);

      assert.equal(next.inCurrent.avg.val, 15);
      assert.equal(next.inCurrent.min.val, 10);
      assert.equal(next.inCurrent.cnt.val, 2);

      var testReading3 = {
        inCurrent: 12,
        outCurrent: 7
      };

      var next = collector._calculateMetric(testReading3, next);

      assert.equal(next.outCurrent.avg.val.toFixed(2), (35/3).toFixed(2));
      assert.equal(next.outCurrent.tot.val, 35);
      assert.equal(next.outCurrent.max.val, 20);

      var testReading4 = {
        solar: 11
      };

      var next = collector._calculateMetric(testReading4, next);

      assert.equal(next.outCurrent.avg.val.toFixed(2), (35/3).toFixed(2));
      assert.equal(next.outCurrent.tot.val, 35);
      assert.equal(next.outCurrent.max.val, 20);
      assert.equal(next.solar.avg.val, 11);
    });

  });

  describe('calculate', function () {

    it('should calculate a correct next values', function () {

      var args = ['tot', 'val', 'cnt'];
      var pred = 'return { tot: (tot+val), cnt: (cnt+1), val: (tot+val)/(cnt+1) }';
      var calcAverage = new Function(args, pred);

      var res1 = calcAverage(20, 10, 1);
      assert.equal(res1.val, 15);

      var res2 = calcAverage(10, res1.tot, res1.cnt);
      assert.equal(res2.val.toFixed(2), (40 / 3).toFixed(2));

      var res3 = calcAverage(20, res2.tot, res2.cnt);
      assert.equal(res3.val.toFixed(2), (60 / 4).toFixed(2));

      var res4 = calcAverage(3, res3.tot, res3.cnt);
      assert.equal(res4.val.toFixed(2), (63 / 5).toFixed(2));

      var res5 = calcAverage(22.5, res4.tot, res4.cnt);
      assert.equal(res5.val.toFixed(2), ((63+22.5) / 6).toFixed(2));

    });


    it('should calculate a correct new total', function () {

      var val1 = 10, val2 = 20, count = 1, total = 0;

      var calcTotal = function (val1, val2) {
        total = val1 + val2;
        return {
          val: total
        };
      };

      var res1 = calcTotal(val1, val2);
      assert.equal(res1.val, 30);
      var res2 = calcTotal(res1.val, val1);
      assert.equal(res2.val, 40);
      var res3 = calcTotal(res2.val, val2);
      assert.equal(res3.val, 60);

    });

    it('should calculate Kwh correctly', function() {
      var reading = {
        solar: 10,
        outVoltage: 24
      }; // 240W
      var ts1 = collector._getTimestamp();
      var initialized = collector._initializeDoc(metrics, reading);
      console.log(util.inspect(initialized, {showHidden: false, depth: null}));

      assert.equals(initialized.solarpower.toth.val, 0);

      var reading1 = {
        solar: 20,
        outVoltage: 24
      }; // 480W

      setTimeout(function () {
        var ts2 = collector._getTimestamp();
        var next = collector._calculateMetric(reading1, initialized);
        assert.equal(next.solarpower.avg.val, 15);
        assert.equal(next.solarpower.toth.val, 480/3600);
      },1000)

    })
  });
});