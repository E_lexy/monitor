/**
 * Created by alex on 6/01/16.
 */
const
// utilities
  _ = require('lodash'),
  moment = require('moment'),
  assert = require('assert');

var
  clientList = [];

module.exports = function (config, eventEmitter, Https) {

  var io = require('socket.io')(Https);

  return {
    // socket.io
    init: function (db) {
      io.sockets.on('connection', function (socket) {
        //console.log('a user connected', socket.handshake.headers.host);

        socket.on('api', function (msg) {

          console.log(msg);

          sendHistory = function(coll, from, to) {
            db.findDocuments(db, coll, {
              from: from,
              to: to
            }, function (records) {
              socket.emit('powerhistory', 'start');
              // Execute find on all the documents
              var result = {period: 'day', record: data};
              socket.emit('powerhistory', records);

              socket.emit('powerhistory', 'done');

            });
          };
          switch (msg.request) {

            case 'powerhistory#day':
              sendHistory('minute',
                moment().subtract(24, 'hours').format('x'),
                moment().format('x'));
              break;
            case 'powerhistory#6h':
              sendHistory('minute',
                moment().subtract(6, 'hours').format('x'),
                moment().format('x'));
              break;
            case 'powerhistory#12h':
              sendHistory('minute',
                moment().subtract(12, 'hours').format('x'),
                moment().format('x'));
              break;
            case 'powerhistory#1h':
              sendHistory('10seconds',
                moment().subtract(1, 'hours').format('x'),
                moment().format('x'));
              break;
          }
        });

        clientList.push(socket.id);
        socket.on('disconnect', function () {
          var i = clientList.indexOf(socket.id);
          clientList.splice(i, 1);
        });

      });

      var objReading = {};
      var sendReading = function (reading) {
        //console.log('send reading...');
        _.assign(objReading, reading);
        objReading.timestamp = moment().format('x');
        io.sockets.emit(config.api.readingChannel, objReading);
      };

      eventEmitter.on('reading', sendReading);

    }
  };

};
