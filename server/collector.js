/**
 * Created by alex on 6/01/16.
 */
const
// utilities
  util = require('util'),
  _ = require('lodash'),
  moment = require('moment'),
  assert = require('assert'),
  cron = require('cron');

var
  CronJob = cron.CronJob,
  reading = {},
  lastReading = {},
  timeZone = 'Europe/Madrid',
  currentMetric,
  written = {},
  hour = 60 * 60,
  day = 60 * 60 * 24,
  granularities = [
    {name: '10seconds', schema: '*/10 * * * * *', exp: hour},
    {name: 'minute', schema: '0 * * * * *', exp: day * 7},
    {name: 'hour', schema: '0 0 * * * *', exp: day * 370},
    {name: 'day', schema: '0 0 0 * * *', exp: day * 1000},
    {name: 'week', schema: '0 0 0 * * 0', exp: day * 1000},
    {name: 'month', schema: '0 0 0 1 * 0', exp: day * 1000}
  ],
  metrics = [
    {name: 'outCurrent', granularities: ['10seconds', 'minute'], operations: ['average']},
    {name: 'inCurrent', granularities: ['minute'], operations: ['average']},
    {name: 'batVoltage', granularities: ['10seconds', 'minute', 'hour'], operations: ['average']},
    {name: 'batCurrent', granularities: ['10seconds', 'minute', 'hour'], operations: ['average']},
    {name: 'outVoltage', granularities: ['minute'], operations: ['average']},
    {name: 'inVoltage', granularities: ['minute'], operations: ['average']},
    {name: 'inFreq', granularities: ['minute'], operations: ['average']},
    {
      name: 'invPower',
      calculation: {
        args: ['outCurrent', 'outVoltage'],
        pred: 'return outCurrent * outVoltage'
      },
      granularities: ['10seconds', 'minute', 'hour', 'day', 'week', 'month'],
      operations: ['average', 'max']
    },
    {
      name: 'genPower',
      calculation: {
        args: ['inCurrent', 'inVoltage'],
        pred: 'return inCurrent * inVoltage'
      },
      granularities: ['10seconds', 'minute', 'hour', 'day', 'week', 'month'],
      operations: ['average', 'max']
    },
    {
      name: 'solarPower',
      calculation: {
        args: ['solar', 'outVoltage'],
        pred: 'return (solar * outVoltage) / 1000'
      },
      granularities: ['minute', 'hour', 'day', 'week', 'month'],
      operations: ['average', "totalhour"]
    },
    {
      name: 'solar',
      granularities: ['10seconds', 'minute'], operations: ['average', 'total']
    }
  ];


var calculatedProperty = function(metric, reading) {
  var func = new Function(metric.calculation.args, metric.calculation.pred);
  var arrArgs = [];
  for (i = 0; i < metric.calculation.args.length; i++) {
    var strVal = metric.calculation.args[i];
    var val = eval('reading.' + strVal);
    if(val == undefined || val === null) val = 0;
    // console.log('val: ',val);

    arrArgs.push(val);
  }
  return func.apply(null, arrArgs);
};

var initializeDoc = function (metrics, reading) {
  var obj = {},
    _getReading = function(name) {
      if(reading[name] == undefined || reading[name] === null) {
        return 0;
      } else {
        return reading[name];
      }
    };

  _.each(metrics, function (metric) {
    obj[metric.name] = {};

    if (metric.calculation) {
      reading[metric.name] = calculatedProperty(metric, reading);
    }


    _.each(metric.operations, function (op) {

      var operationObj = {};
      switch (op) {
        case 'average':
          operationObj = {
            op: 'avg',
            val: _getReading(metric.name),
            //tot: reading[metric.name],
            cnt: _getReading(metric.name) ? 1 : 0,
            formula: {
              args: ['nw', 'val', 'cnt'],
              pred: 'return { cnt: cnt + 1, val : ((val * cnt) + nw) / (cnt+1) }'
            }
          };
          break;
        case 'total':
          operationObj = {
            op: 'tot',
            val: _getReading(metric.name),
            formula: {
              args: ['nw', 'val'],
              pred: 'return { val : val + nw }'
            }
          };
          break;
        case 'totalhour':
          operationObj = {
            op: 'toth',
            val: _getReading(metric.name),
            formula: {
              args: ['nw', 'pt', 'val'],
              pred: 'return { val : (val + nw) / (pt*3600000) ) }'
            }
          };
          break;
        case 'max':
          operationObj = {
            op: 'max',
            val: _getReading(metric.name),
            formula: {
              args: ['nw', 'val'],
              pred: 'return { val : nw > val ? nw : val }'
            }
          };
          break;
        case 'min':
          operationObj = {
            op: 'min',
            val: _getReading(metric.name),
            formula: {
              args: ['nw', 'val'],
              pred: 'return { val : nw < val ? nw : val }'
            }
          };
          break;
        case 'count':
          operationObj = {
            op: 'cnt',
            val: 1,
            formula: {
              args: ['val'],
              pred: 'return { val : val + 1 }'
            }
          };
          break;
      }

      obj[metric.name][operationObj.op] = operationObj;

    });
  });

  obj.timestamp = getTimestamp();
  obj.start = getTimestamp();

  return obj;
};

var getTimestamp = function() {
  return moment().format('x');
};

var calculateMetric = function (reading, last) {

  // console.log('reading:',reading, 'last: ',last);
  _.each(last, function (metricObj, metricName) {
      // console.log('metricName: ',metricName);

      if(!(metricName in reading)) {
        // console.log('metric ['+metricName+'] not in reading, moving on...');
        return;
      }

      if(metricName.indexOf('_') === 0 || metricName === 'timestamp') {
        // console.log('moving on...');
        return; // move on to next
      }
      var definedMetric = _.find(metrics, {name: metricName});
      if (definedMetric && definedMetric.calculation) {
        reading[metricName] = calculatedProperty(definedMetric, reading);
      }

    _.each(metricObj, function (operation, opName) {

      // console.log('opName: ',opName);

      if (operation.formula) {
        // console.log('operation: ', operation);
        var func = new Function(operation.formula.args, operation.formula.pred);
        var arrArgs = [];

        for (i = 0; i < operation.formula.args.length; i++) {
          var strVal = operation.formula.args[i];

          if (strVal === 'nw') {
            // get new value for reading as predicate param
            // console.log('nw '+strVal+': ',reading[metricName]);
            var val = reading[metricName];
            if(val == undefined || val === null) val = 0;
          } else if (strVal === 'pt') {

            // get time delta in milliseconds for reading as predicate param
            // console.log('nw '+strVal+': ',reading[metricName]);
            if(val == undefined || val === null) {
              console.log('no last timestamp to calculate delta T');
            } else {
              var val = last.timestamp - getTimestamp();
            }
          } else {

            // get the operations' value of the last objectfor the metric
            var val = eval('operation.' + strVal);
            // console.log('else operation.'+strVal+': ',val);
          }

          // push the argument in the array
          arrArgs.push(val);

        }
        // console.log(func.toString());

        var nextObj = func.apply(null, arrArgs);
        // console.log('nextObj: ', nextObj);

        operation = _.assign(operation, nextObj);
        // console.log('next operation obj: ', operation);

      } else {

      }
    });
  });

  // console.log('last: ',last);

  return last;

};

var aggregateReadingForGranularity = function (persistence, reading, granularity, aggregate) {
  var readingDocument = {};
  var granu = _.find(granularities, {name: granularity});

  if (granu) {
    persistence.getIntermediateReading(granu, function (lastReading) {
      readingDocument = calculateMetric(reading, lastReading);
      readingDocument.timestamp = getTimestamp();

      if (_.keys(readingDocument).length > 2) {
        persistence.setIntermediateReading(readingDocument, granu, function (result) {
          // console.log('stored intermediate result for: ' + granularity, result);
        }, function (err) {
          console.log(err);
        });
      }

    }, function (err) {

      persistence.insertIntermediateReading(initializeDoc(aggregate, reading), granu, function (result) {
         // console.log('stored INITIALIZED result for : ' + granularity, result);
      }, function (err) {
        console.log(err);
      });

    });

  } else {
    console.log(granularity + ' is invalid.');
  }

};

var getMetricsByAggregation = function (metrics) {
  var arrAggregations = {};

  _.each(metrics, function (metric) {

    _.each(metric.granularities, function (granularity) {

      if (!_.isArray(arrAggregations[granularity])) arrAggregations[granularity] = [];

      if (!_.find(arrAggregations[granularity], metric)) {
        arrAggregations[granularity].push(metric);
      }

    });
  });

  return arrAggregations;
};

var arrAggregations = getMetricsByAggregation(metrics);

module.exports = function (config, eventEmitter) {

  var multi = require('./readers/multi')(config, eventEmitter);
  var solar = require('./readers/solarpower')(config, eventEmitter);

  return {

    reading: {},

    init: function (persistence) {

      // start collection polling processes
      // will generate events with data
      multi.init(config);
      solar.init(config);

      var aggregate = function (reading) {
        _.each(arrAggregations, function (aggregate, key) {
          aggregateReadingForGranularity(persistence, reading, key, aggregate);
        });
      };

      // trigger calculation on each reading
      eventEmitter.on('reading', function (reading) {
        aggregate(reading);
        console.log('reading: >>>>>>>>>>>>>>>>> ');
        console.log(reading);
        exports.reading = reading;
      });

      _.each(granularities, function (granularity) {
        new CronJob(granularity.schema, function () {

            if (granularity.name == 'minute') {
              eventEmitter.emit('checknotify', exports.reading);
            }

            var readingMetricsForGranularity = _.get(arrAggregations, granularity.name);

            if (exports.reading) {
              persistence.insertIntermediateReading(
                initializeDoc(readingMetricsForGranularity, exports.reading), granularity, function (result) {
                console.log('started next period for [' + granularity.name + ']', result);
                // console.log(util.inspect(result, {showHidden: false, depth: null}));
              }, function (err) {
                console.log(err);
              });
            }

          }, function () {
            /* This function is executed when the job stops */
          },
          true, /* Start the job right now */
          timeZone /* Time zone of this job. */
        );
      });


    },

    _initializeDoc: initializeDoc,
    _calculateMetric: calculateMetric,
    _getTimestamp: getTimestamp

  }
};
