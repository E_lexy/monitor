/**
 * Created by alex on 6/01/16.
 */
const
// main includes
  Pushover = require('node-pushover-client'),
// main utilities
  _ = require('lodash'),
  moment = require('moment'),
  assert = require('assert');

const
  BAT_VOLTAGE_HIGH = 32.2,
  BAT_VOLTAGE_LOW = 23.2,
  BAT_CURRENT_HIGH = 70,
  INV_POWER_HIGH = 2900,
  GEN_POWER_HIGH = 1500;

var pushNotification = new Pushover({
  token: 'aUDAVD2baGHmm2yGEb7LvLzn2HKumR',
  user: '9vngEBNxKLO7lsKMB3G7UwEec3R5Ni'
});

var config = {};

var check = function (reading) {
  if(reading) {
    if (reading.batVoltage > BAT_VOLTAGE_HIGH) {
      send('Battery Voltage too HIGH: ' + reading.batVoltage, 2);
    }
    if (reading.batVoltage < BAT_VOLTAGE_LOW) {
      send('Battery Voltage too LOW: ' + reading.batVoltage, 2);
    }
    // if (reading.batCurrent > BAT_CURRENT_HIGH) {
    //   send('Battery Current HIGH: ' + reading.batCurrent, -2);
    // }
    // if (reading.invPower > INV_POWER_HIGH) {
    //   send('Inverter Power HIGH: ' + reading.invPower, -2);
    // }
    if (reading.genPower > GEN_POWER_HIGH) {
      send('Generator Power HIGH: ' + reading.genPower, -2);
    }
  }
};

var send = function (message, prio) {
  var notifybject = {
    message: message,
    priority: prio,
    timestamp: moment().format('X'),
    url: config.host || ''
  };
  if (prio === 2) {
    notifybject.expire = 3600;
    notifybject.retry = 5;
  }
  var req = pushNotification.send(notifybject);

  req.then(function (res) {
    console.log(res);
  });

};

module.exports = function (cfg, eventEmitter) {

  config = cfg;
  eventEmitter.on('checknotify', check);

  return {

    check: check,
    send: send
  };
};
