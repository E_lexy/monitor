/**
 * Created by alex on 6/01/16.
 */
const
  Request = require('request'),
// utilities
  _ = require('lodash'),
  moment = require('moment'),
  assert = require('assert');


module.exports = function (config, eventEmitter) {
  return {

    init: function () {

      setInterval(function () {
        Request(config.reader.inverter.mk2daemon, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            var currentTimestamp = moment();

            var reading = _.merge(JSON.parse(body), {timestamp: currentTimestamp.format('x')});
            //console.log(reading);
            reading.type='multi';

            eventEmitter.emit('reading', reading);

          } else {
            console.log(error);
          }
        });
      }, config.reader.inverter.polling_frequency);

    }
  }
};
