/**
 * Created by alex on 6/01/16.
 */
const
  Request = require('request'),
// utilities
  _ = require('lodash');


module.exports = function (config, eventEmitter) {
  return {

    init: function () {

      setInterval(function () {
        Request(config.reader.tracer.daemon, function (error, response, body) {
          if (!error && response.statusCode == 200) {
            if(body) {
              var reading = JSON.parse(body);

              var objReading = {};
              _.forEach(reading.values, function (metric) {
                objReading[metric.key] = metric.value;
              });

              eventEmitter.emit('reading', {tracer: objReading});
            }
          } else {
            console.log(error);
          }
        });
      }, config.reader.tracer.polling_frequency);

    }
  }
};