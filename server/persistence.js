/**
 * Created by alex on 6/01/16.
 */
// main utilities
var
  _ = require('lodash'),
  moment = require('moment'),
  assert = require('assert');

const
  READING_PREFIX = 'readings_';

var mongodb = false;

var ensureExpiry = function (collection, granularity) {
  collection.indexExists("timestamp_1", function (err, result) {
    //console.log(result);
    if (!result) {

      console.log('expiration for [' + collection.collectionName + ']: ' + granularity.exp);

      collection.createIndex({timestamp: 1}, {
          expireAfterSeconds: granularity.exp,
          unique: true,
          background: true,
          w: 1
        }, function (err, indexName) {
          if (err) {
            console.log(err);
          } else {
            console.log(indexName);
          }
        });
    }
  });
};

module.exports = function (config, eventEmitter) {
  return {
    db: {},

    init: function (successCb, errorCb) {
      // var url = 'mongodb://' + config.mongo.host + ':' + config.mongo.port + '/' + config.mongo.name;
      // var MongoClient = Mongo.MongoClient;
      //
      // MongoClient.connect(url, function (err, db) {
      //   //    assert.equal(null, err);
      //   if (err) {
      //     console.log('\u001b[31mFailed to connect to MongoDB: ' + err + '\033[0m');
      //     errorCb(err);
      //   } else {
      //
      //   }
      // });
      var Engine = require('tingodb')();

      var dataDir = config.db_path || '../data';
      var db = new Engine.Db(dataDir, {});
      console.log("Opened TongoDB");
      exports.db = db;
      successCb();
    },

    getIntermediateReading: function (granularity, successCb, errorCb) {
      // Get the documents collection
      var collName = READING_PREFIX + granularity.name;
      var collection = exports.db.collection(collName);

      //console.log('getting for [' + collName + ']: ');

      // Find some documents
      collection.find().sort({timestamp: -1}).limit(1).toArray(function (err, docs) {
        if (err) {
          console.log(err);
          errorCb();
        } else {
          if(docs.length && docs[0] && _.isObject(docs[0][Object.keys(docs[0])[0]])) {
            successCb(docs[0]);
          } else {
            console.log('old style doc found in collection : ' + collName, docs[0]);
            errorCb();
          }
        }
      });
    },

    setIntermediateReading: function (reading, granularity, successCb, errorCb) {

      //console.log('storing for [' + granularity.name + ']: ', moment().format());

      var collName = READING_PREFIX + granularity.name;
      var collection = exports.db.collection(collName);

      ensureExpiry(collection, granularity);
      collection.update({ _id: reading._id}, {$set: reading}, function (err, result) {
        if (err) {
          console.log(err);
          errorCb();
        } else {
          successCb(result);
        }
      });
    },

    insertIntermediateReading: function (reading, granularity, successCb, errorCb) {

      //console.log('storing for [' + granularity.name + ']: ', moment().format());

      var collName = READING_PREFIX + granularity.name;
      var collection = exports.db.collection(collName);

      ensureExpiry(collection, granularity);

      collection.insert([
        reading
      ], function (err, result) {
        if (err) {
          console.log(err);
          errorCb();
        } else {
          successCb(result);
        }
      });
    },

    findDocuments: function (mongodb, collSuffix, oPeriod, successCb, errorCb) {
      // Get the documents collection
      var collName = READING_PREFIX + collSuffix;
      var collection = exports.db.collection(collName);

      //console.log('getting for [' + collName + ']: ');

      var query = {
        timestamp: {$gt: oPeriod.from, $lt: oPeriod.to}
      };

      //console.log(query);

      // Find some documents
      collection.find(query, {timestamp: 1, batVoltage: 1, invPower:1 })
        .limit(60 * 24).sort({timestamp: -1}).toArray(function (err, docs) {
        if (err) {
          console.log(err);
          errorCb();
        } else {
          successCb(docs);
        }
      });
    },

    findDocumentsStream: function (mongodb, collSuffix, oPeriod, successCb, errorCb) {
      // Get the documents collection
      var collName = READING_PREFIX + collSuffix;
      var collection = exports.db.collection(collName);

      //console.log('getting for [' + collName + ']: ');

      var query = {
        timestamp: {$gt: oPeriod.from, $lt: oPeriod.to}
      };

      console.log(query);

      // Find some documents
      var stream = collection.find(query, {timestamp: 1, batVoltage: 1, invPower:1 })
        .sort({timestamp: -1}).stream();

      successCb(stream);
    }
  };
};
