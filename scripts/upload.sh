#!/bin/bash

URL=$1
LOC=$2

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
rsync -e ssh  -avlO --progress --exclude-from $DIR/exclude.txt --delete $DIR/.. $URL:$LOC
