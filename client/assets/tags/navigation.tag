<navigation>

  <!-- .nav -->
  <ul class="nav navbar-nav">
    <li class={route=="dashboard" ? "active" : ""}>
      <a href="#">Dashboard</a>
    </li>
    <li class={route=='energy' ? 'active' : ''}>
      <a href="/#energy">Power</a>
    </li>
    <li class={route=='heating' ? 'active' : ''}>
      <a href="/#heating">Heating</a>
    </li>
    <li class={route=='water' ? 'active' : ''}>
      <a href="/#water">Water</a>
    </li>
  </ul><!-- /.nav -->


  <script>

    var self = this;

    riot.route(function (section) {
      switch (section) {
        case 'energy':
          self.route = 'energy';
          break;
        case 'heating':
          self.route = 'heating';
          break;
        case 'water':
          self.route = 'water';
          break;
        default:
          self.route = 'dashboard';
      }
      console.log('active', self.route);
      self.update();
    })

  </script>
</navigation>
