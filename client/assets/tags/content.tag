<content>

  <div class="outer">
    <div class="inner bg-light lter">

      <div show={ route=='dashboard' }>
        <dashboard-content></dashboard-content>
      </div>

      <div show={ route=='energy' }>
        <power-content></power-content>
      </div>

      <div show={ route=='heating' }>
        <heating-content></heating-content>
      </div>

      <div show={ route=='water' }>
        <water-content></water-content>
      </div>

    </div><!-- /.inner -->
  </div><!-- /.outer -->

  <script>

    self = this;

    riot.route(function (section) {
      switch (section) {
        case 'energy':
          self.route = 'energy';
          break;
        case 'heating':
          self.route = 'heating';
          break;
        case 'water':
          self.route = 'water';
          break;
        default:
          self.route = 'dashboard';
      }
      console.log('route: ',self.route);
      self.update();
    });

    riot.route.start(true);

  </script>

</content><!-- /#content -->
