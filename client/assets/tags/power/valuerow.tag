<valuerow>

  <div class="row">

    <div class="col-xs-12 col-sm-6 col-lg-3">
      <gaugewidget metric={metrics.bc}></gaugewidget>
    </div>

  </div>

  <div class="row">

    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.bv}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.bc}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.ip}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.gp}></valuewidget>
    </div>

  </div>

  <script>
    this.metrics =
    {
      bv: {
        name: 'batVoltage',
        title: 'Battery',
        unit: 'volts',
      },
      bc: {
        name: 'solar',
        title: 'Solar',
        unit: 'Amps',
      },
      ip: {
        name: 'invPower',
        title: 'Inverter',
        unit: 'Watt',
      },
      gp: {
        name: 'genPower',
        title: 'Genset',
        unit: 'Watt',
      }
    }
  </script>

</valuerow>
