<readingtable>

  <div class="box">
    <div class="body">
      <table class="table table-condensed table-hovered sortableTable tablesorter tablesorter-default" role="grid">
        <thead>
        <tr role="row">
          <th>Metric</th>
          <th>Value</th>
        </tr>
        </thead>
        <tbody>
        <tr each={ metric, value in items } class="active" role="row">
          <td>{metric}</td>
          <td if={ metric!='timestamp' }>{value}</td>
          <td if={ metric=='timestamp' }>{ moment(Number(value)).format('HH:m:s') }</td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>

  <script>

    var self = this;
    self.items = [];

    Monitor.on('data', function (data) {

      console.log(data);

      self.items = data;

      self.update();
    });

  </script>

</readingtable>