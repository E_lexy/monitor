<power-content>

  <valuerow></valuerow>

  <hr>

  <div class="row">

    <div class="col-lg-8">
      <chart></chart>
    </div>

    <div class="col-lg-4">
      <readingtable></readingtable>
    </div>

  </div>
</power-content>