<chart>

  <div class="btn-group" role="group" aria-label="...">
    <button type="button" class="btn btn-default" onclick={ chart1h }>1h</button>
    <button type="button" class="btn btn-default" onclick={ chart6h }>6h</button>
    <button type="button" class="btn btn-default" onclick={ chart12h }>12h</button>
  </div>

  <div class="box">
    <header>
      <h5>Power last 12h</h5>
    </header>
    <div class="body" id="dashboard-chart" style="height: 250px;"></div>
  </div>

  <script>
    var batVoltageData = [],
        invPowerData = [],
        solarPowerData = [],
        nbPoints = 60 * 24,
        chart;

    this.on('mount', function() {
      chart = new CanvasJS.Chart("dashboard-chart",
          {

            title: {
              text: "Power Graph"
            },
            zoomEnabled: true,
            data: [
              {
                type: "spline",
                xValueType: "dateTime",
                dataPoints: batVoltageData
              },
              {
                type: "spline",
                xValueType: "dateTime",
                axisYType: "secondary",
                dataPoints: invPowerData
              },
              {
                type: "spline",
                xValueType: "dateTime",
                axisYType: "secondary",
                dataPoints: solarPowerData
              }
            ]
          }
      );

      var limitChart = function (collection, i) {
        if (collection.length > nbPoints) {
          chart.options.data[i].pop();
        }
      };

      socket.on('powerhistory', function (historyData) {
        if (historyData === 'start') {
          console.log('start received history');
        } else if (historyData === 'done') {
          console.log('received all the history');
        } else {
          if (historyData.period === 'day') {

            var batVoltageValue = 0;
            if (_.isObject(historyData.record.batVoltage)) {
              batVoltageValue = historyData.record.batVoltage.avg.val;
            } else {
              batVoltageValue = historyData.record.batVoltage;
            }
            if (batVoltageValue > 18 && batVoltageValue < 40) {
              chart.options.data[0].dataPoints.push(
                  {
                    x: Number(historyData.record.timestamp),
                    y: Number(batVoltageValue)
                  }
              );
            }

            var invPowerValue = 0;
            if (_.isObject(historyData.record.invPower)) {
              invPowerValue = historyData.record.invPower.avg.val;
            } else {
              invPowerValue = historyData.record.invPower;
            }
            if (invPowerValue < 3500) {
              chart.options.data[1].dataPoints.push(
                  {
                    x: Number(historyData.record.timestamp),
                    y: Number(invPowerValue)
                  }
              );
            }

            chart.render();
          }

        }
      });

      var getInverterPower = function (reading) {
        return (reading.outVoltage * reading.outCurrent).toFixed(2);
      };
      var getGeneratorPower = function (reading) {
        return (reading.inVoltage * reading.inCurrent).toFixed(2);
      };
      var getSolarPower = function (reading) {
        return (reading.outVoltage * reading.solar).toFixed(2);
      };

      window.setInterval(function () {

        chart.options.data[0].dataPoints.unshift(
            {
              x: Number(Monitor.readingData.timestamp),
              y: Number(Monitor.readingData.batVoltage)
            }
        );
        limitChart(0);


        chart.options.data[1].dataPoints.unshift(
            {
              x: Number(Monitor.readingData.timestamp),
              y: Number(getInverterPower(Monitor.readingData))
            }
        );
        limitChart(1);

        chart.options.data[2].dataPoints.unshift(
            {
              x: Number(Monitor.readingData.timestamp),
              y: Number(getSolarPower(Monitor.readingData))
            }
        );
        limitChart(1);

        chart.render();

      }, 5000);

    });

    this.chart1h = function () {
      resetChart();
      chart.options.title.text = 'History 1h';
      nbPoints = 60;
      socket.emit('api', {request: 'powerhistory#1h'});
    };
    this.chart6h = function () {
      resetChart();
      nbPoints = 60 * 6;
      chart.options.title.text = 'History 6h';
      socket.emit('api', {request: 'powerhistory#6h'});
    };
    this.chart12h = function () {
      resetChart();
      nbPoints = 60 * 12;
      chart.options.title.text = 'History 12h';
      socket.emit('api', {request: 'powerhistory#12h'});
    };
    this.chart24h = function () {
      resetChart();
      nbPoints = 60 * 24;
      chart.options.title.text = 'History 24h';
      socket.emit('api', {request: 'powerhistory#day'});
    };

    resetChart = function() {
      chart.options.data[0].dataPoints = [];
      chart.options.data[1].dataPoints = [];
      chart.render();
    }

  </script>

</chart>
