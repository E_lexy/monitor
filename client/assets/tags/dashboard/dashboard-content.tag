<dashboard-content>
  <div class="row">

    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.bv}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.bc}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.bp}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.ip}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.gp}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.ie}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.ce}></valuewidget>
    </div>
    <div class="col-xs-12 col-sm-6 col-lg-3">
      <valuewidget metric={metrics.s1}></valuewidget>
    </div>

  </div>

  <div class="" row>
    <div class="col-lg-4">
      <readingtable></readingtable>
    </div>
  </div>

  <script>
    this.metrics =
    {
      bv: {
        name: 'batVoltage',
        title: 'DC',
        unit: 'volts',
      },
      bc: {
        name: 'batCurrent',
        title: 'DC',
        unit: 'Amp',
      },
      bp: {
        name: 'batPower',
        title: 'DC',
        unit: 'Watt',
      },
      ip: {
        name: 'invPower',
        title: 'Inverter',
        unit: 'Watt',
      },
      gp: {
        name: 'genPower',
        title: 'Genset',
        unit: 'Watt',
      },
      ie: {
        name: 'invEfficiency',
        title: 'Inverter %',
        unit: '%',
      },
      ce: {
        name: 'crgEfficiency',
        title: 'Charger %',
        unit: '%',
      },
      s1: {
        name: 'solar',
        title: 'Solar',
        unit: 'Amps',
      }
    }
  </script>

</dashboard-content>
