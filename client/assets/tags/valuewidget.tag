<valuewidget>

  <div class="stats_box">
    <div class="inline sparkline">loading....</div>
    <div class="stat_text">
      <strong>{ opts.metric.title }</strong>{ opts.metric.unit }
      <span class="{ style }">{ lastValue }</span>
    </div>
  </div>

  <script>

    var self = this;
    self.lastValue = 0;
    self.values = [];
    self.metric = opts.metric.name;
    self.style = 'percent same';

    Monitor.on('data', function (data) {

      self.lastValue = Number(data[self.metric]);
      var prevValue = Number(self.values[self.values.length - 1]);
      if (prevValue < self.lastValue) {
        self.style = 'percent up';
      } else if (prevValue > self.lastValue) {
        self.style = 'percent down';
      }

      self.values.push(self.lastValue);
      if (self.values.length > 20) self.values.shift();

      $('.inline.sparkline', self.root).sparkline(self.values, {
        type: 'line',
        height: '40',
        drawNormalOnTop: false
      });

      self.update();
    });

  </script>

</valuewidget>
