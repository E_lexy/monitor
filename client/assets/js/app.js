;(function ($, Monitor) {
  var $button = $('.inner a.btn');
  Monitor.metisButton = function () {
    $.each($button, function () {
      $(this).popover({
        placement: 'bottom',
        title: this.innerHTML,
        content: this.outerHTML,
        trigger: (Monitor.isTouchDevice) ? 'touchstart' : 'hover'
      });
    });
  };
  return Monitor;
})(jQuery, Monitor || {});

;(function ($) {
  "use strict";
  Monitor.dashboard = function () {


    //----------- BEGIN SPARKLINE CODE -------------------------*/
    // required jquery.sparkline.min.js*/

    /** This code runs when everything has been loaded on the page */
    /* Inline sparklines take their values from the contents of the tag */
    $('.inlinesparkline').sparkline();

    /* Sparklines can also take their values from the first argument
     passed to the sparkline() function */
    var myvalues = [10, 8, 5, 7, 4, 4, 1];
    $('.dynamicsparkline').sparkline(myvalues);

    /* The second argument gives options such as chart type */
    $('.dynamicbar').sparkline(myvalues, {type: 'bar', barColor: 'green'});

    /* Use 'html' instead of an array of values to pass options
     to a sparkline with data in the tag */
    $('.inlinebar').sparkline('html', {type: 'bar', barColor: 'red'});


    $(".sparkline.bar_week").sparkline([5, 6, 7, 2, 0, -4, -2, 4], {
      type: 'bar',
      height: '40',
      barWidth: 5,
      barColor: '#4d6189',
      negBarColor: '#a20051'
    });

    $(".sparkline.line_day").sparkline([5, 6, 7, 9, 9, 5, 4, 6, 6, 4, 6, 7], {
      type: 'line',
      height: '40',
      drawNormalOnTop: false
    });

    $(".sparkline.pie_week").sparkline([1, 1, 2], {
      type: 'pie',
      width: '40',
      height: '40'
    });

    $('.sparkline.stacked_month').sparkline(['0:2', '2:4', '4:2', '4:1'], {
      type: 'bar',
      height: '40',
      barWidth: 10,
      barColor: '#4d6189',
      negBarColor: '#a20051'
    });
    //----------- END SPARKLINE CODE -------------------------*/
  };
  return Monitor;
})(jQuery);

;(function (Monitor) {
  Monitor.readingData = {};

  var getInverterPower = function (reading) {
    return (reading.outVoltage * reading.outCurrent).toFixed(2);
  };
  var getGeneratorPower = function (reading) {
    return (reading.inVoltage * reading.inCurrent).toFixed(2);
  };
  var getBatteryPower = function (reading) {
    return (reading.batVoltage * reading.batCurrent).toFixed(2);
  };
  var getInverterEffeciency = function (reading) {
    return ((getInverterPower(reading) - getGeneratorPower(reading)) / getBatteryPower(reading)).toFixed(2);
  };
  var getChargerEffeciency = function (reading) {
    if (getGeneratorPower(reading)) {
      return ((getBatteryPower(reading) - getInverterPower(reading)) / getGeneratorPower(reading)).toFixed(2);
    } else {
      return 0;
    }
  };

  var getNetDC = function(reading) {
    return (parseFloat(reading.solar)-parseFloat(reading.batCurrent)).toFixed(2);
  }

  socket.on('reading_channel', function (reading) {

    if(reading.solar < 1) {
      reading.solar = 0;
    }
    reading.batCurrent = getNetDC(reading);
    if (reading.outCurrent > 100) reading.outCurrent = reading.outCurrent / 100;

    _.assign(Monitor.readingData,reading);

    reading.invPower = getInverterPower(Monitor.readingData);
    reading.genPower = getGeneratorPower(Monitor.readingData);
    reading.batPower = getBatteryPower(Monitor.readingData);
    reading.invEfficiency = getInverterEffeciency(Monitor.readingData);
    reading.crgEfficiency = getChargerEffeciency(Monitor.readingData);

    Monitor.trigger('data', reading);
  });

})(Monitor || {});
