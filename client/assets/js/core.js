/**
 * Monitor - Bootstrap-Admin-Template v2.3.2
 * Author : onokumus
 * Copyright 2015
 * Licensed under MIT (https://github.com/onokumus/Bootstrap-Admin-Template/blob/master/LICENSE.md)
 */

;(function (window) {
  var
  // Is Modernizr defined on the global scope
    Modernizr = typeof Modernizr !== "undefined" ? Modernizr : false,
  // whether or not is a touch device
    isTouchDevice = Modernizr ? Modernizr.touch : !!('ontouchstart' in window || 'onmsgesturechange' in window),
  // Are we expecting a touch or a click?
    buttonPressedEvent = (isTouchDevice) ? 'touchstart' : 'click',
    Monitor = function () {
      riot.observable(this);
      this.init();
    };

  // Initialization method
  Monitor.prototype.init = function () {
    this.isTouchDevice = isTouchDevice;
    this.buttonPressedEvent = buttonPressedEvent;
  };

  Monitor.prototype.getViewportHeight = function () {

    var docElement = document.documentElement,
      client = docElement.clientHeight,
      inner = window.innerHeight;

    if (client < inner)
      return inner;
    else
      return client;
  };

  Monitor.prototype.getViewportWidth = function () {

    var docElement = document.documentElement,
      client = docElement.clientWidth,
      inner = window.innerWidth;

    if (client < inner)
      return inner;
    else
      return client;
  };

  // Creates a Monitor object.
  window.Monitor = new Monitor();
})(this);
;(function ($, Monitor) {
  "use strict";
  // Define toggleFullScreen
  Monitor.toggleFullScreen = function () {
    if ((window.screenfull !== undefined) && screenfull.enabled) {
      $('#toggleFullScreen').on(Monitor.buttonPressedEvent, function (e) {
        screenfull.toggle(window.document[0]);
        $('body').toggleClass('fullScreen');
        e.preventDefault();
      });
    } else {
      $('#toggleFullScreen').addClass('hidden');
    }
  };
  // Define boxFullScreen
  Monitor.boxFullScreen = function () {
    if ((window.screenfull !== undefined) && screenfull.enabled) {
      $('.full-box').on(Monitor.buttonPressedEvent, function (e) {
        var $toggledPanel = $(this).parents('.box')[0];
        screenfull.toggle($toggledPanel);
        $(this).parents('.box').toggleClass('full-screen-box');
        $(this).parents('.box').children('.body').toggleClass('full-screen-box');
        $(this).children('i').toggleClass('fa-compress');
        e.preventDefault();
      });
    } else {
      $('.full-box').addClass('hidden');
    }
  };
  return Monitor;
})(jQuery, Monitor || {});

;(function ($) {
  $(document).ready(function () {

    $('[data-toggle="tooltip"]').tooltip();

    //$('#menu').metisMenu();
    Monitor.toggleFullScreen();

  });
})(jQuery);
